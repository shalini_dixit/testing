import setuptools

setuptools.setup(
    name="mytestpackage",
    version="0.0.1",
    author="Shalini Dixit",
    author_email="shalinidxt0@gmail.com",
    description="mytestpackage v0.1 code",
    long_description_content_type="text/markdown",
    url="https://gitlab.com/shalini_dixit/testing.git",
    packages=setuptools.find_packages()
)